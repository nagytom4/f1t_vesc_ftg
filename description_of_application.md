# Popis aplikácie BLDC-tool
### Všeobecné informácie
- Chyby nájdené v aplikácií:
-- V sekcií "real time data" popisy na x-ovej osi neodpovedajú skutočným hodnotám, skutočné hodnoty sú 5krát menšie.
-- Keď riadime motor pomocou FOC riadenia a máme nastavený open loop, tak vyzerá, že integračná zložka uravretej slučky
integruje aj v čase keď ešte beží otvorená slučka.

### Motor configuration
##### Motor
- **Motor type**
-- BLDC/DC/FOC - typ riadenia motora
- **Current limits** - podľa parametrov morota a baterky
-- Motor max: maximála hodnota prúdu, ktorú môžeme dať do motora. Je dobré ju nastaviť o trochu menej ako je maximálna povolená záťaž motora.
-- Motor min (regen): -toto je maximálny bzrdný prúd.
-- Batt max: maximálna hodnota prúdu, aký chceme aby baterka dodala. Prúd nesmie byť väčší ako maximum čo vie baterka dodať. (Maximum čo vie baterka dodať vypočítame ako: kapacita * vybíjací prúd ) 
-- Batt min (regen): -VESC podporuje tzv. rekuperáciu, čiže pri bzdení vracia energiu naspäť do baterky. Toto obmedzenie nastavuje aký najväčší prúd môže VESC pri brzdení vracať nazad do baterky
-- Absolute max: Je kontrolovaný každý PWM cyklus. Žiadny z prúdov nesmie prekročiť túto hodnotu prúdu, ak ju prekročí, VESC vyhodí chybu a zastavý motor. Nastavuje sa vyššie ako ostatné, pretože je preferovaná metóda "soft back-off" pred vypínaním motora.
-- Slow absolute max: Ak je tento parameter zapnutý, "Absolute max" prúd sa filtruje a používa sa v prípadoch, keď je na motore veľa šumu a ochrana "Absolute max" zasahuje stále aj v prípadoch kedy by nemusela. Väčšinou sa ale aj tak zaškrtáva.
- **RPM limits (BLDC only)**
-- Maximálne hodnoty ERPM na aké bude vedieť VESC nastaviť motor
-- ERPM - Engine Revolutions per Minute
- **Voltage limits** - dôležité kvôli ochrane baterky
-- Minimum / Maximum input voltage - pokiaľ napätie na baterke nebude v týchto medziach, VESC nespustí motor a vyhlási chybu vstupného napätia
--Battery cutoff start / end: keď napätie na baterke klesne pod Battery cutoff start, VESC značne obmedzí výkon motoru. Pokiaľ napätie klesne pod Battery cutoff end, VESC motor úplne vypne. Pokiaľ nastavíme hodnoty na rovnakú hodnotu, VESC neobmedzí výkon, ale rovno vypne motor.
- **Temperature limits**
- Keď teplota presiahne start, VESC obmedzí výkon. Keď teplota presiahne end, VESC úplne vypne motor. Meranie teploty na motore vyžaduje pripojenie senzoru teploty k VESCu.
- **Other limits** 
-- Minimum/Maximum duty cycle: keď riadime motor cez duty cycle, tak toto sú jeho maximálne limity. minimálny treba nastaviť tak veľký, aby sa motor zvládal točiť. Maximálny duty cycle môže byť až 1 ale ak je treba, môžeme ho takto obmedziť na menej. 
Pozn.: Pri nastavovaní limít treba dávať pozor a limity nastaviť správne. Ak sa nastavia zle, môže to poškodiť ESC.

##### BLDC
- **Sensor mode**
-- Sensorless/Sensored/Hybrid: Sensorless - na riadenie nie je používaný senzor. Senzored - používané spätnoväzebné riadenie so senzorom. Hybrid - pri rozbehu sa riadi podľa senzoru, potom sa prepne na sensorless mode. ERPM pri ktorom prebehne prepnutie sa nastavý v sekcií BLDC-Hall sensor-Sensorless ERPM (hybrid mode).
- **Sensorless**
-- Min ERPM: minimálny ERPM keď by sa mal motor začať točiť. Nedávať moc malé, lebo motor nemá pod určitú hodnotu dosť výkonu na to, aby sa začal točiť.
-- BR ERPM: je RPM od ktorého vyššie je používané nastavenie phase advance.
-- Phase advance at BE RPM: "1"-žiaden fázový predstih, "0"-maximálny (30 deg) fázový predstih. Toto nastavenie trochu vylepšuje správanie motora vo vyšších otáčkach, ale nie je až tak dôležité.
- **Sensorless - commutation mode**
-- Comm mode: integrate/delay treba vyskúšať ktoré funguje na danom motore lepšie, ale väčšinou sa využíva integrate. Delay mode sa používa na nízko indukčných vysokorýchlostných motorch v prípade, že integračný mód nefunguje.
-- Integrator limit a BECF Coupling sa nastavuje pomocou automatického testu v aplikácií. 
Pozn. aby automatické testy fungovali dobre, musia byť správne nastavené limity napätia v sekcií motor configuration-motor a motor musí bežať naprázdno, nemôže mať na sebe ani malú záťaž. V prípade autíčka je nutné z neho motor vybrať, inak bude test hádzať error. Motor treba dobre na niečo uchytiť, pretože pri tomto teste sa motor roztočí veľkou rýchlosťou. Ak tomu tak nieje, treba nastaviť parametre pri teste (väčšinou treba niektorý z parametrov zväčšiť). 
--Int limit min ERPM: malo by byť okolo 1000 - 2000, nastavenie tohoto parametru nie je až tak dôležité pre riadenie motora. Ale obecne, ak sa tento parameter zníži, bude motor bungovať lepšie, ak má záťaž veľkú zotrvačnosť. 
- **Hall sensors**
-- Table: treba nastaviť, ak sa používajú halové sondy na určovanie polohy motoru. Ak sú pripojené, aplikácia by ich mala zaznamenať a tabuľku nastaviť.

##### FOC
Základné nastavenia v tejto časti fungujú pre väčšinu motorov. Jediné, čo je treba nastaviť sú parametre, ktoré sa zisťujú pomocou predprogramovaných testov. Opäť treba, aby motor bežal naprázdno bez hociakej záťaže. Je nutné aby testy boli robené v danom poradí.
Najprv treba stlačiť tlačidlo Measure R and L, toto zmeria odpor a indukčnosť motora. Motor iba jemne "zacuká", ale nezačne sa točiť. Ďalším testom v poradí je test ktorý zistí parameter lambda. Pri tomto teste sa motor roztočí, takže ho treba na niečo uchytiť. Nakoniec treba stlačiť Calc CC a potom Calc (pri observer gain).
Parameter, ktorý sa ešte dá jednoducho zistiť a dosť ovplyvní správanie motora je parameter v sekcií General(cont) parameter F_SW. Tento parameter býva väčšinou okolo 20000 - 30000Hz a znamená ako rýchlo sa budú prepínať fázi motoru pri FOC riadení. Je možné, že keď sa tento parameter zväčší, tak bude menej oscilácií vo vysokých otáčkach (ak nejaké sú).

Pozn. Toto sa týka aj BLDC aj FOC módu:
Pokiaľ sa motor po tomto nastavení pri riadení pomocou RPM bude správať "divne", napr. bude dlho nabiehať, bude mať veľký prekmit rýchlosti, nebude vôbec držať rýchlosť, ale bude iba kmitať a stále meniť smer pohybu, tak treba zmeniť nastavenie PID regulátora v sekcií Motor configuration-Motor-Advanced-speed control. Tieto konštanty sa asi najľahšie nastavia experimentálne. Správne nastavenie tohoto regulátoru je kľúčovým parametrom v riadení pomocou RPM.

##### Advanced
- **PWM mode (DC or BLDC only)**
--Synchronous/Bipolar/Nonsynchronous-HISW: mód riadenia motoru, odporúčané použiť Synchronous, čo je najlepšia možnosť pre väčšinu motorov. Bipolar je lepší iba pre motory keď sa motor v móde "Synchronous" správa divne. Nonsynchronous mód sa neoporúča, tento mód je v testovaciom režime a môže sa stať, že použitie tohoto módu odpáli ESC.
- **Current control** - nastavenie regulátora pre riadenie prúdom
-- Startup boost: je minimálny "duty cycle" ktorý ptreba použiť na roztočenie motoru. Väčšinou je v hodnotách 0.01 až 0.1, ale maximálny rozsah je 0 až 1. 
-- Min current: je minimálny povolený prúd do motora. Väčšinou okolo 0.8A.
-- Gain (BLDC and DC only): je to gain (proporcionálna zložka regulátoru). Keď sa toto naspavenie zväčší bude rýchlešia prúdova odozva na zmenu, ale keď sa nastavý moc veľkú hodnotu, tak sa môže systém stať nestabilný a hrozí poškodenie ESC. Základné nastavenie funguje relatívne dobre pre väčšinu motorov. 
- **Speed control** - správne nastavenie tohoto regulátoru nutné, aby fungovalo správne riadenie pomocou RPM.
-- KP: - proporcionálna zložka regulátoru
-- KI: - integračná zložka regulátoru
-- KD: - derivačná zložka regulátoru - !nedávať moc veľké! zosilňuje šum a moc veľká hodnota tohoto parametru môže poškodiť ESC.
-- Min ERPM: je minimálne RPM pri akom sa má motor začať točiť. Hociaký príkaz na beh motora, ktorého RPM je pod touto hodnotou VECS odignoruje. Treba otestovať aká je najmenšia hodnota RPM akou sa motor ešte dokáže točiť a na takú hodnotu treba nastaviť tento parameter.
- **Misc**
-- Fault stop time: je čas v milisekundách na akú dobu sa má VESC kompletne vypnúť pri nejakej chybe. Až po tomto čase bude VESC znova odpovedať na príkazy.
- **Hall/encoder port mode**
Pokiaľ sa použije na určovanie poloby rotora senzor, tu treba nastaviť typ senzoru a komunikáciu s ním.
- **Position control** - správne nastavenie tohoto regulátoru nutné, aby fungovalo správne riadenie pomocou uhla.
-- KP: - proporcionálna zložka regulátoru
-- KI: - integračná zložka regulátoru
-- KD: - derivačná zložka regulátoru - !nedávať moc veľké! zosilňuje šum a moc veľká hodnota tohoto parametru môže poškodiť ESC.
-- Angle division: najmenší uhol o ktorý sa má byť schopný motor otočiť.
- **Backoff and ramping (DC and BLDC only)**
-- Duty ramp step (at 1 kHz): Nastavenie rampy pri riadení cez duty cycle. Ako najviac sa môže zmeniť duty cycle za jeden update. Táto hodnota sa updatuje 1000 krát za sekundu. (čím menšia hodnota, dlhší nábeh) Odporúčané nastavovať spôsobom, že nastavíme na veľmi malú hodnotu a zväčšujeme kým nieje priebeh nábehu postačujúci.
-- Max current ramp step (at 1 kHz): Nastavenie rampy pri riadení cez prúd. Funguje podobne ako duty cycle.
-- Current backoff gain: základné nastavenie funguje pre väčšinu motorov
-- Speed limit ramp step: Nastavenie rampy pri riadení cez RPM. Funguje podobne ako duty cycle.

