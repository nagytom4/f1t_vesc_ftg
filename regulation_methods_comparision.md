# Porovnanie vytvorených regulátorov
Všetky parametre VESCu a postupy nastavovania sú uvedené v "description_of_application.md". Dôležitá časť nastavenia VESCu je ale správne nastavenie PID regulátora v uzavretej slučke, ktorá zabezpečuje riadenie pomocou RPM. Regulátor, ktorý je treba nastaviť sa nachádza v BLDC-tool -> motor configuration -> advanced -> speed control. Tento regulátor je potrebný aj pre BLDC aj pre FOC riadenie motora.
***
Regulátory som nastavoval experimentálne. Vyskúšal som viac rôznnych nastavení a v tomto dokumente porovnám ich vlastnosti a výhody oproti pôvodnému nastaveniu. Typy regulátorov a riadenia, ktoré som skúšal sú uvedené v nasledujúcej tabuľke.

| Ozančenie | Typ regulátora        | Typ riadenia  |   P     |   I     |   D     | poznámka                            |
|:---------:| :-------------------: |:-------------:|:-------:|:-------:|:-------:| :---------------------------------: |
| RO        | PI                    | BLDC          | 0,03200 | 0,10000 |    0    |  originálne nastavenie              |
| R1        | PI                    | BLDC          | 0,00020 | 0,00050 |    0    |  veľké oscilácie                    |
| R2        | PID                   | FOC           | 0,02300 | 0,01000 | 0,00180 |   V1                                |
| R3        | PD                    | FOC           | 0,00800 |    0    | 0,00100 |  nemá nulovú ustálenú odchýlku - V2 |
| R4        | PID                   | FOC           | 0,00500 | 0,00200 | 0,00070 |   V3                                |

Pozn. V tabuľke nie je zahrnutá možnosť regulátoru PI v FOC móde a PD, PID v BLDC móde, to je z dôvodu, že v FOC riadení pridanie derivačnej zložky značne zlepšilo správanie (znížilo vybrácie), preto som regulátory bez derivačnej zložky v FOC nebral v úvahu. PD a PID regulátor v BLDC móde riadenia motora, som taktiež nezahrnul, pretože pridanie derivačnej zložky správanie regulátoru zhoršilo.

***

![R0](./full_batt/starup_800RPM/Original_BLDC.png)
Regulátor RO
![R1](./full_batt/starup_800RPM/BLDC_V1.png)
Regulátor R1
![R2](./full_batt/starup_800RPM/FOC_V1.png)
Regulátor R2
![R3](./full_batt/starup_800RPM/FOC_V2.png)
Regulátor R3
![R4](./full_batt/starup_800RPM/FOC_V3.png)
Regulátor R4

# Porovnanie BLDC vs FOC (field oriented control)
Riadenie motora FOC módom bolo značne jednoduchšie a motor bežal oveľa tichšie. Pomocou BLDC módu riadenia sa mi ani nepodarilo riadiť motor bez veľkých oscilácií (\pm 500 ERPM) v ustálenom stave. Na presné riadenie auta aj pri malých otáčkach sa jednoznačne hodí viac FOC riadenie.